
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * The graphical interface
 * @author kraus
 */
public class FXMain extends Application {
    final static int SIZE = 65;
    final static int HEIGHT = 8;
    final static int WIDTH = 8;
    private Group tiles = new Group();
    static Group pieces = new Group();
    static AbstractGraphicalPiece[][] gPieceList = new AbstractGraphicalPiece[8][8];
    static Group buttons = new Group();
    static RectangleButton[] buttonList = new RectangleButton[4];
    static Text[] textList = new Text[4];
    static Group texts = new Group();
    static String[] buttonLabels = {"Cust", "Save", "Load", "Start"};
    static Group rectangles = new Group();
    static Rectangle[] rectangleList = new Rectangle[2];
    static Group timeTexts = new Group();
    static TimeText[] timeTextList = new TimeText[2];
    static Group endGameTextG = new Group();
    static Text endGameText;
    ButtonController buttonController = new ButtonController();
    /**
     * Creates the graphical interface, fills it with all components
     * @return created board
     */
    private Parent createScene() {
        
        Pane board = new Pane();
        board.setPrefSize((WIDTH + 1) * SIZE + 6, HEIGHT * SIZE + SIZE);
        board.getChildren().addAll(tiles);
        board.getChildren().addAll(pieces);
        board.getChildren().addAll(buttons);
        board.getChildren().addAll(texts);
        board.getChildren().addAll(rectangles);
        board.getChildren().addAll(timeTexts);
        board.getChildren().addAll(endGameTextG);
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                Tile tile = new Tile(new Coord(x, y));
                tile.findYourPlace();
                tiles.getChildren().add(tile);
                
                if (Main.chessboard[x][y].isReal) {
                    AbstractGraphicalPiece piece = new AbstractGraphicalPiece(new Coord(x, y), Main.chessboard[x][y].color, Main.chessboard[x][y].type);
                    piece.findYourPlace();
                    gPieceList[x][y] = piece;
                    pieces.getChildren().add(piece);
                }
            }
        }
        for (int x = 0; x < 4; ++x) {
            RectangleButton button = new RectangleButton(8 * SIZE + 6, x * SIZE * 2 + 2 * x, x);
            button.findYourPlace();
            buttonList[x] = button;
            MyText text = new MyText(8 * SIZE, x * SIZE * 2, x, buttonLabels[x]);
            text.findYourPlace();
            /*texts.getChildren().addAll(agent, text);*/
            buttons.getChildren().add(button);
            texts.getChildren().add(text);
        }
        rectangleList[0] = new Rectangle();
        rectangleList[0].setFill(Color.WHEAT);
        rectangleList[0].setWidth(SIZE * 4);
        rectangleList[0].setHeight(SIZE);
        rectangleList[0].relocate(0, SIZE * 8 + 6);
        rectangles.getChildren().add(rectangleList[0]);
        
        rectangleList[1] = new Rectangle();
        rectangleList[1].setFill(Color.MAROON);
        rectangleList[1].setWidth(SIZE * 4);
        rectangleList[1].setHeight(SIZE);
        rectangleList[1].relocate(SIZE * 4, SIZE * 8 + 6);
        rectangles.getChildren().add(rectangleList[1]);
        
        timeTextList[0] = new TimeText(100, 8 * SIZE + 20, Team.WHITE);
        timeTextList[0].findYourPlace();
        timeTexts.getChildren().add(timeTextList[0]);
        timeTextList[1] = new TimeText(4 * SIZE + 100, 8 * SIZE + 20, Team.BLACK);
        timeTextList[1].findYourPlace();
        timeTexts.getChildren().add(timeTextList[1]);
        
        return board;
    }
    /**
     * Refreshes the chessboard (only pieces)
     */
    static void restart() {
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                pieces.getChildren().remove(gPieceList[x][y]);
                if (Main.chessboard[x][y].isReal) {
                    AbstractGraphicalPiece piece = new AbstractGraphicalPiece(new Coord(x, y), Main.chessboard[x][y].color, Main.chessboard[x][y].type);
                    piece.findYourPlace();
                    gPieceList[x][y] = piece;
                    pieces.getChildren().add(piece);
                }
            }
        }
    }
    /**
     * Removes a grafical piece
     * @param pos position of the removed piece
     */
    static void removePiece(Coord pos) {
        pieces.getChildren().remove(gPieceList[pos.x][pos.y]);
    }
    /**
     * Moves a graphical piece
     * @param oldCoord old position of the moved piece
     * @param newCoord new position of the moved piece
     */
    static void movePiece(Coord oldCoord, Coord newCoord) {
        gPieceList[oldCoord.x][oldCoord.y].move(newCoord);
        gPieceList[newCoord.x][newCoord.y] = gPieceList[oldCoord.x][oldCoord.y];
        gPieceList[oldCoord.x][oldCoord.y] = null;
    }
    /**
     * Prints which player won
     * @param losingTeam team who lose
     */
    static void endGame(Team losingTeam) {
        endGameText = new Text();
        endGameText.relocate(SIZE / 3, SIZE * 4);
        endGameText.setFont(new Font(100));
        endGameText.setFill(Color.BLUE);
        endGameText.setText((losingTeam == Team.WHITE ? Team.BLACK : Team.WHITE) + " LOST");
        endGameTextG.getChildren().add(endGameText);
        
    }
    /**
     * Starts the the stage
     * @param primaryStage 
     */
    @Override
    public void start(Stage primaryStage) {
        Scene scene = new Scene(createScene());
        primaryStage.setTitle("Chess");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        primaryStage.setOnCloseRequest((WindowEvent event) -> {
            if (ButtonController.timerExists) {
                ButtonController.timer.stop();
            }
            if (Main.serverExists) {
                Main.server.stop();
            }
            System.out.println("Stage is closing");
        });
        
    }
    
    /**
     * The main functions
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

