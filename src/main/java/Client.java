import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * A client that sends information to the other players receiver
 * @author kraus
 */
public class Client {
    static Boolean readyToSend = true;
    private Socket socket;
    /**
     * Creates a socket
     * @param serverAddress address of the other PC
     * @param serverPort Port of the other PCs receiver
     * @throws Exception 
     */
    Client(InetAddress serverAddress, int serverPort) throws Exception {
        this.socket = new Socket(serverAddress, serverPort);
    }
    /**
     * Starts the client - gets its port and prints it
     * @throws Exception 
     */
    public void startClient() throws Exception {
        System.out.println("\r\nConnected to Server: " + socket.getInetAddress());
    }
    /**
     * sends a text message to the other players receiver
     * @param message text message to send
     */
    void send(String message) {
        PrintWriter out;
        try {
            out = new PrintWriter(this.socket.getOutputStream(), true);
            out.println(message);
            out.flush();
            System.out.println("tried");
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
}