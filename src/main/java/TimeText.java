
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


/**
 * Graphical representation of timer, also holds the current value. Updates when
 * the update method is called (by Timer).
 * @author kraus
 */
public class TimeText extends Text {
    int coordX;
    int coordY;
    Team color;
    Boolean locked = false;
    /**
     * sets its position, text, font and color
     */
    void findYourPlace() {
        relocate(coordX, coordY);
        setText(timeToString((color == Team.WHITE ? Main.whiteTime : Main.blackTime)));
        setFont(Font.font ("Verdana", 19));
        if (color == Team.WHITE) {
            setFill(Color.MAROON);
        } else {
            setFill(Color.WHEAT);
        }
        
    }
    /**
     * updates the time by substracting one second
     */
    void update() {
        if (!locked) {
            if (color == Team.WHITE) {
                Main.whiteTime--;
                setText(timeToString(Main.whiteTime));
            }
            if (color == Team.BLACK) {
                Main.blackTime--;
                setText(timeToString(Main.blackTime));
            }
            
        }
        if (Main.whiteTime == 0) {
            Main.endGame(Team.WHITE);
            locked = true;
        }
        if (Main.blackTime == 0) {
            Main.endGame(Team.BLACK);
            locked = true;
        }
    }

    /**
     * Transforms a time in second into a text formate consisting od minutes and seconds
     */
    String timeToString(int time) {
        int minutes = time / 60;
        int seconds = time - 60 * minutes;
        String ret = minutes + ":" + seconds;
        return ret;
    }
    
    public TimeText(int x, int y, Team color) {
        this.coordX = x;
        this.coordY = y;
        this.color = color;
    }
}