
/**
 * Portable inforation about game, meant to be saved and loaded easily.
 * @author kraus
 */
public class PortableInfo {
    private int blackTime;
    private int whiteTime;
    private Team whoseTurn;
    private Coord enPassant;

    public PortableInfo(int whiteTime, int blackTime, Team whoseTurn, Coord enPassant) {
        this.blackTime = blackTime;
        this.whiteTime = whiteTime;
        this.whoseTurn = whoseTurn;
        this.enPassant = enPassant;
    }
    public PortableInfo() {
        
    }

    public int getBlackTime() {
        return blackTime;
    }

    public int getWhiteTime() {
        return whiteTime;
    }

    public Team getWhoseTurn() {
        return whoseTurn;
    }

    public void setBlackTime(int blackTime) {
        this.blackTime = blackTime;
    }

    public void setWhiteTime(int whiteTime) {
        this.whiteTime = whiteTime;
    }

    public void setWhoseTurn(Team whoseTurn) {
        this.whoseTurn = whoseTurn;
    }

    public Coord getEnPassant() {
        return enPassant;
    }

    public void setEnPassant(Coord enPassant) {
        this.enPassant = enPassant;
    }
    

    @Override
    public String toString() {
        return "PortableInfo{" + "blackTime=" + blackTime + ", whiteTime=" + whiteTime + ", whoseTurn=" + whoseTurn + ", enPassant=" + enPassant + '}';
    }
    
    
}
