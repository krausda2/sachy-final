/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Enum which represents type of chess piece.
 * @author david
 */
public enum Type {
    ROOK, BISHOP, QUEEN, KING, KNIGHT, PAWN, PLACEHOLDER
}
