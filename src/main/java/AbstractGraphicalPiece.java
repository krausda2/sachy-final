
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;



/**
 * Represents the graphical representation of piece
 * @author kraus
 */
public class AbstractGraphicalPiece extends StackPane {
    public Coord coord;
    public Team color;
    public Type pieceType;
    public Image image1;
    /**
    * Relocates depending on its coordinates. Also chooses external picture
    * depending on its type.
    */
    void findYourPlace() {
        relocate(coord.x * FXMain.SIZE, coord.y * FXMain.SIZE);
        
        final ImageView selectedImage = new ImageView();
        if (color == Team.WHITE) {
            if (null != pieceType) switch (pieceType) {
                case PAWN:
                    image1 = new Image(Main.class.getResourceAsStream("wpawn.png"));
                    break;
                case BISHOP:
                    image1 = new Image(Main.class.getResourceAsStream("wbishop.png"));
                    break;
                case KING:
                    image1 = new Image(Main.class.getResourceAsStream("wking.png"));
                    break;
                case QUEEN:
                    image1 = new Image(Main.class.getResourceAsStream("wqueen.png"));
                    break;
                case ROOK:
                    image1 = new Image(Main.class.getResourceAsStream("wrook.png"));
                    break;
                case KNIGHT:
                    image1 = new Image(Main.class.getResourceAsStream("wknight.png"));
                    break;
                default:
                    break;
            }
        } else {
            if (null != pieceType) switch (pieceType) {
                case PAWN:
                    image1 = new Image(Main.class.getResourceAsStream("bpawn.png"));
                    break;
                case BISHOP:
                    image1 = new Image(Main.class.getResourceAsStream("bbishop.png"));
                    break;
                case KING:
                    image1 = new Image(Main.class.getResourceAsStream("bking.png"));
                    break;
                case QUEEN:
                    image1 = new Image(Main.class.getResourceAsStream("bqueen.png"));
                    break;
                case ROOK:
                    image1 = new Image(Main.class.getResourceAsStream("brook.png"));
                    break;
                case KNIGHT:
                    image1 = new Image(Main.class.getResourceAsStream("bknight.png"));
                    break;
                default:
                    break;
            }
        }
        selectedImage.setImage(image1);
        getChildren().addAll(selectedImage);
    }
    /**
    * Relocates itself to designated position
     * @param target target position
    */
    void move(Coord target) {
        relocate(target.x * FXMain.SIZE, target.y * FXMain.SIZE);
        coord = new Coord(target.x, target.y);
    }
    /**
    * Constructor, also sets mousePressed event
     * @param position its initial position
     * @param team its team (color)
     * @param type its type
    */
    public AbstractGraphicalPiece(Coord position, Team team, Type type) {
        coord = new Coord(position.x, position.y);
        color = team;
        pieceType = type;
        setOnMousePressed((MouseEvent click) -> {
            InputController.move(TypeOfPlayer.THISPC, Main.whoseTurn, coord);
        });
    }
    
    
}