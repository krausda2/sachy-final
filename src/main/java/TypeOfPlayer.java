/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Enum that represents type of player.
 * @author kraus
 */
public enum TypeOfPlayer {
    THISPC, REMOTEPC, RANDOM
}
