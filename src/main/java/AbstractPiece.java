
/**
 * Abstract piece, other pieces implements this class, so all the pieces can be
 * stored in the same array.
 * @author david
 */
public abstract class AbstractPiece {
    Team color;
    String name;
    Type type;
    /**
    * Indicates if the piece has moved in this game. Useful for castling or for
    * pawns.
    */
    Boolean hasMoved;
    /**
    * Indicats if the piece is real playable piece (false if placeholder)
    */
    Boolean isReal;
    
    public AbstractPiece(Team color, String name, Type type, Boolean hasMoved, Boolean isReal) {
        this.color = color;
        this.name = name;
        this.type = type;
        this.hasMoved = hasMoved;
        this.isReal = isReal;
    }
    /**
    * Returns all positions the piece is able to go to (does not consider other pieces)
     * @param coord its current coordinates (pieces dont remember they own coordinates)
     * @return returns list of possible moves
    */
    abstract Coord[][] getMoves(Coord coord);

    public Team getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public Boolean getHasMoved() {
        return hasMoved;
    }

    public Boolean getIsReal() {
        return isReal;
    }

    public void setColor(Team color) {
        this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setHasMoved(Boolean hasMoved) {
        this.hasMoved = hasMoved;
    }

    public void setIsReal(Boolean isReal) {
        this.isReal = isReal;
    }

    
    @Override
    public String toString() {
        return "Is real: " + isReal + ", color: " + color + ", name: " + name + ", type: " + type + ", has moved: " + hasMoved + '\n';
    }

    
}
