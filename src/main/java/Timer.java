
import javafx.application.Platform;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class notifies every second the timer of current player to update.
 * @author kraus
 */
class Timer implements Runnable {
   private Thread t;
   private Boolean wait = false;

   @Override
   /**
 * This class notifies every second the timer of current player to update.
 */
   public void run() {
      try {
         while(true) {
             if (Main.gameRunning) {
                 Platform.runLater(new Runnable() {
                    @Override public void run() {
                        if (Main.whoseTurn == Team.WHITE) {
                            FXMain.timeTextList[0].update();
                        } else {
                            FXMain.timeTextList[1].update();
                        }
                    }
                });
                 
             }
             Thread.sleep(1000);
         }
      } catch (InterruptedException e) {
         System.out.println("Thread interrupted.");
      }
   }
   /**
    * ends the tread
    */
   public void stop() {
        Thread moribund = t;
        t = null;
        moribund.interrupt();
    }
   /**
    * starts the tread
    */
   public void start () {
      if (t == null) {
         t = new Thread (this);
         t.start ();
      }
   }
}
