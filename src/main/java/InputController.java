
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * Contolls all moves and relocations - if its really the moving players turn, 
 * if the relocation mode is on, ...
 * Also synchronizes random players and remote players
 * @author kraus
 */
public class InputController {
    static TypeOfPlayer whitePlayer;
    static TypeOfPlayer blackPlayer;
    static RandomPlayer randomPlayer = new RandomPlayer();
    /**
     * A constructor, decides which player has which color
     * @param white a number that indicates white players type
     * @param black a number that indicates black players type
     */
    InputController(int white, int black) {
        
        if (white == 1) {
            InputController.whitePlayer = TypeOfPlayer.THISPC;
        } else if (white == 2) {
            InputController.whitePlayer = TypeOfPlayer.RANDOM;
            
        } else if (white == 3) {
            InputController.whitePlayer = TypeOfPlayer.REMOTEPC;
            
        }
        if (black == 1) {
            InputController.blackPlayer = TypeOfPlayer.THISPC;
        } else if (black == 2) {
            InputController.blackPlayer = TypeOfPlayer.RANDOM;
        } else if (black == 3) {
            InputController.blackPlayer = TypeOfPlayer.REMOTEPC;
            
        }
    }
    /**
     * Transform a coordinate to send to a string with object mapper and sends it
     * to the client
     * @param coord a coordinate to be sent (it can represent something else than coordinate) 
     */
    static void sender(Coord coord) {
        ObjectMapper objectMapper = new ObjectMapper();
        String coordAsString;
        try {
            coordAsString = objectMapper.writeValueAsString(coord);
            Main.client.send(coordAsString);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(InputController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Asks a random player to play
     * @param team the random players color
     */
    static void moveRequester(Team team) {
        while (Main.whoseTurn == team) {
            randomPlayer.move(team);
        }
    }
    /**
     * Controls if the move request comes from the right player, also request moves
     * from random player if exists, also sends info to remote player if exists
     * @param who which type of player is requesting the move
     * @param whichColor color of the requester
     * @param coord a coordinates (initial or target)
     */
    static void move(TypeOfPlayer who, Team whichColor, Coord coord){
        if (Main.adjustingBoard) {
            Main.relocate(coord);
        } else {
            if (whitePlayer == TypeOfPlayer.THISPC && blackPlayer == TypeOfPlayer.THISPC) {
                Main.move(coord);
            } else if (whitePlayer == TypeOfPlayer.THISPC && blackPlayer == TypeOfPlayer.RANDOM) {
                if (Main.whoseTurn == Team.WHITE && who == TypeOfPlayer.THISPC) {
                    Main.move(coord);
                    if (Main.whoseTurn == Team.BLACK) {
                        moveRequester(Team.BLACK);
                    }
                } else {
                    Main.move(coord);
                }
            } else if (whitePlayer == TypeOfPlayer.THISPC && blackPlayer == TypeOfPlayer.REMOTEPC) {
                if (who == TypeOfPlayer.THISPC) {
                    if (Main.whoseTurn == Team.WHITE) {
                        sender(coord);
                        Main.move(coord);
                        System.out.println("sent");
                    }
                } else {
                    if (Main.whoseTurn == Team.BLACK) {
                        Main.move(coord);
                    }
                }
            } else if (whitePlayer == TypeOfPlayer.RANDOM && blackPlayer == TypeOfPlayer.THISPC) {
                if (Main.whoseTurn == Team.BLACK && who == TypeOfPlayer.THISPC) {
                    Main.move(coord);
                    if (Main.whoseTurn == Team.WHITE) {
                        moveRequester(Team.WHITE);
                    }
                } else {
                    Main.move(coord);
                }
            } else if (whitePlayer == TypeOfPlayer.REMOTEPC && blackPlayer == TypeOfPlayer.THISPC) {
                if (who == TypeOfPlayer.THISPC) {
                    if (Main.whoseTurn == Team.BLACK) {
                        sender(coord);
                        Main.move(coord);
                        System.out.println("sent");
                    }
                } else {
                    if (Main.whoseTurn == Team.WHITE) {
                        Main.move(coord);
                    }
                }
            }
        }
    }
    @Override
    public String toString() {
        return "InputController{" + "white=" + whitePlayer + ", black=" + blackPlayer + '}';
    }
    
    
}
