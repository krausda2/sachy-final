/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Enum which represent team (color)
 * @author david
 */
public enum Team {
    BLACK,
    WHITE
}
