
import java.util.List;
import java.util.Random;


/**
 *
 * @author kraus
 */
public class RandomPlayer {
    /**
     * Moves a random piece to a random destination
     * @param team Team of the piece which should move
     */
    static void move(Team team) {
        Coord[] ret = new Coord[2];
        List<Coord> moves;
        Random rand = new Random();
        int x = rand.nextInt(7);
        int y = rand.nextInt(7);
        if(Main.chessboard[x][y].isReal && Main.chessboard[x][y].color == team) {
            moves = Main.checkMoves(new Coord(x, y), team);
            if (!moves.isEmpty()) {
                ret[0] = new Coord(x, y);
                ret[1] = moves.get(rand.nextInt(moves.size()));
                while (ret[1] == null) {
                    ret[1] = moves.get(rand.nextInt(moves.size()));
                }
                System.out.println(ret[0] + " + " + ret[1]);
                InputController.move(TypeOfPlayer.RANDOM, team, ret[0]);
                InputController.move(TypeOfPlayer.RANDOM, team, ret[1]);
            }
        }
    }
}
