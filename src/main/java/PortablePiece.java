

/**
 * This class can hold all parameters of a real piece and uses an universal
 * constructor (unlike real pieces), so pieces saved like this are easy to load
 * with json.
 * @author kraus
 */
public class PortablePiece {
    Team color;
    String name;
    Type type;
    Boolean hasMoved;
    Boolean isReal;
    
    public PortablePiece(Team color, String name, Type type, Boolean hasMoved, Boolean isReal) {
        this.color = color;
        this.name = name;
        this.type = type;
        this.hasMoved = hasMoved;
        this.isReal = isReal;
    }
    public PortablePiece() {
        
    }
    /**
    * Create real piece using parameters of this portable piece.
    */
    AbstractPiece toPiece() {
        if (null == type) {
            return new Placeholder(color, name, type, hasMoved, isReal);
        } else switch (type) {
            case KING:
                return new King(color, name, type, hasMoved, isReal);
            case QUEEN:
                return new Queen(color, name, type, hasMoved, isReal);
            case BISHOP:
                return new Bishop(color, name, type, hasMoved, isReal);
            case KNIGHT:
                return new Knight(color, name, type, hasMoved, isReal);
            case ROOK:
                return new Rook(color, name, type, hasMoved, isReal);
            case PAWN:
                return new Pawn(color, name, type, hasMoved, isReal);
            default:
                return new Placeholder(color, name, type, hasMoved, isReal);
        }
    }
    public Team getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public Boolean getHasMoved() {
        return hasMoved;
    }

    public Boolean getIsReal() {
        return isReal;
    }

    public void setColor(Team color) {
        this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setHasMoved(Boolean hasMoved) {
        this.hasMoved = hasMoved;
    }

    public void setIsReal(Boolean isReal) {
        this.isReal = isReal;
    }

    @Override
    public String toString() {
        return "Is real: " + isReal + ", color: " + color + ", name: " + name + ", type: " + type + ", has moved: " + hasMoved + '\n';
    }
}
