
/**
 * Knight piece, see AbstractPiece
 * @author kraus
 */
public class Knight extends AbstractPiece {

    public Knight(Team color, String name, Type type, Boolean hasMoved, Boolean isReal) {
        super(color, name, type, hasMoved, isReal);
    }
    @Override
    Coord[][] getMoves(Coord coord) {
        /*super.getMoves(coord);*/
        Coord[] n1 = new Coord[1];
        Coord[] e1 = new Coord[1];
        Coord[] w1 = new Coord[1];
        Coord[] s1 = new Coord[1];
        Coord[] n2 = new Coord[1];
        Coord[] e2 = new Coord[1];
        Coord[] w2 = new Coord[1];
        Coord[] s2 = new Coord[1];
        
        if (coord.x + 2 <= 7) {
            if (coord.y + 1 <= 7) {
                e1[0] = new Coord(coord.x + 2, coord.y + 1);
            }
            if (coord.y - 1 >= 0) {
                e2[0] = new Coord(coord.x + 2, coord.y - 1);
            }
        }
        if (coord.x - 2 >= 0) {
            if (coord.y + 1 <= 7) {
                w1[0] = new Coord(coord.x - 2, coord.y + 1);
            }
            if (coord.y - 1 >= 0) {
                w2[0] = new Coord(coord.x - 2, coord.y - 1);
            }
        }
        if (coord.y + 2 <= 7) {
            if (coord.x + 1 <= 7) {
                s1[0] = new Coord(coord.x + 1, coord.y + 2);
            }
            if (coord.x - 1 >= 0) {
                s2[0] = new Coord(coord.x - 1, coord.y + 2);
            }
        }
        if (coord.y - 2 >= 0) {
            if (coord.x + 1 <= 7) {
                n1[0] = new Coord(coord.x + 1, coord.y - 2);
            }
            if (coord.x - 1 >= 0) {
                n2[0] = new Coord(coord.x - 1, coord.y - 2);
            }
        }
        return new Coord[][]{n1, e1, w1, s1, n2, e2, w2, s2};
    }
}
