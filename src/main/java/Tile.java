
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;



/**
 * Graphical representation of tile.
 * @author kraus
 */
public class Tile extends Rectangle {
    public Coord coord;
    public Team color = Team.BLACK;
    
    /**
    * Chooses its fill depending on its position and relocates itself.
    */
    public void findYourPlace() {
        if ((coord.x + coord.y) % 2 == 0) {
            color = Team.WHITE;
        } else {
            color = Team.BLACK;
        }
        setFill(color == Team.BLACK ? Color.MAROON : Color.WHEAT);
        relocate(coord.x * FXMain.SIZE, coord.y * FXMain.SIZE);
    }
    
     /**
    * Constructor
    */
    public Tile(Coord coord) {
        setWidth(FXMain.SIZE);
        setHeight(FXMain.SIZE);
        this.coord = coord;
        setOnMousePressed((MouseEvent click) -> {
            InputController.move(TypeOfPlayer.THISPC, Main.whoseTurn, coord);
        });
    }
    
    
}
