
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.InetAddress;



/**
 * Main class, more information below
 * @author david
 */
class Main {
    /**
    * 2D array that represents chessboard
    */
    static AbstractPiece[][] chessboard = new AbstractPiece[8][8];
    /**
    * Holds information whose turn it is.
    */
    static Team whoseTurn = Team.WHITE;
    /**
    * method move works in two phases, this indicates which phase it is in
    */
    static Boolean pieceChosen = false;
    /**
    * Same as pieceChosen, for method relocate
    */
    static Boolean pieceToRelocateChosen = false;
    /**
    * Remembers which piece was chosen in the first phase of move
    */
    static Coord chosenPiece;
    /**
    * Same as chosenPiece, for method relocate
    */
    static Coord chosenPieceToRelocate;
    /**
    * Remembers moves for method move
    */
    static List<Coord> moves;
    /**
    * White team is in check
    */
    static Boolean whiteCheck;
    /**
    * Black team is in check
    */
    static Boolean blackCheck;
    /**
    * Remembers which piece is threat
    */
    static Coord threat;
    /**
    * Indicates that the board is in customization mode
    */
    static Boolean adjustingBoard = false;
    /**
    * Remaining time of white player
    */
    static int whiteTime = 900;
    /**
    * Remaining time of black player
    */
    static int blackTime = 900;
    /**
     * If true, the game is running and the clocks are ticking
     */
    static Boolean gameRunning = false;
    /**
     * Indicates which piece should be promoted
     */
    static Coord toBePromoted;
    /**
     * Indicatse if the program is waiting for some data, if false, program can continue
     */
    static Boolean waitingForServer = false;
    static Coord enPassant = null;
    /**
     * Main method only calls start method
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.start();
    }
    static Boolean serverExists = false;
    static Boolean clientExists = false;
    static Server server;
    static Client client;
    /**
    * Create the game using other methods
    */
    void start() {
        whiteCheck = false;
        blackCheck = false;
        adjustingBoard = false;
        pieceChosen = false;
        pieceToRelocateChosen = false;
        String[] smthng = {"args", "more"};
        customizePlayers();
        customizeTime();
        fillBoard();
        FXMain.main(smthng);
        
    }
    /**
    * Converts chessboard to portable format
    */
    static PortablePiece[][] chessboardToPortable() {
        PortablePiece[][] ret = new PortablePiece[8][8];
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                ret[x][y] = new PortablePiece(chessboard[x][y].color, 
                        chessboard[x][y].name, chessboard[x][y].type, 
                        chessboard[x][y].hasMoved, chessboard[x][y].isReal);
            }
        }
        return ret;
    }
    /**
     * Loads chessboard from portable format
     * @param portable chessboard in the portable formate
     */
    static void portableToChessboard(PortablePiece[][] portable) {
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                chessboard[x][y] = portable[x][y].toPiece();
            }
        }
    }
    /**
    * loads game from external json file
    */
    static void loadGame() {
        ObjectMapper objectMapper = new ObjectMapper();
        Scanner in = new Scanner(System.in);
        System.out.println("Write the name of the file:\n");
        String name = in.nextLine();
        try {
            PortablePiece[][] portable = new PortablePiece[8][8];
            PortableInfo info;
            portable = objectMapper.readValue(new File("src/" + name + "_chessboard.json"), PortablePiece[][].class);
            info = objectMapper.readValue(new File("src/" + name + "_info.json"), PortableInfo.class);
            portableToChessboard(portable);
            infoIntoChessboard(info);
            FXMain.restart();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Applies information from PortableInfo class to current game.
     * @param info portable object with information
     */
    static void infoIntoChessboard(PortableInfo info) {
        whiteTime = info.getWhiteTime();
        blackTime = info.getBlackTime();
        whoseTurn = info.getWhoseTurn();
        enPassant = info.getEnPassant();
        pieceChosen = false;
        pieceToRelocateChosen = false;
    }
    /**
    * Saves game into json file
    */
    static void saveGame() {
        Scanner in = new Scanner(System.in);
        System.out.println("Write the name of the file:\n");
        String name = in.nextLine();
        ObjectMapper objectMapper = new ObjectMapper();
        PortablePiece[][] portable = chessboardToPortable();
        PortableInfo info = new PortableInfo(whiteTime, blackTime, whoseTurn, enPassant);
        try {
            objectMapper.writeValue(new File("src/" + name + "_chessboard.json"), portable);
            objectMapper.writeValue(new File("src/" + name + "_info.json"), info);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
    * Switching on and off the adjusting mode
    */
    static void switchAdjusting() {
        adjustingBoard = !adjustingBoard;
    }
    /**
    * Lets the user decide which type of players will play
    */
    static void customizePlayers() {
        Scanner in = new Scanner(System.in);
        Boolean playersSet = false;
        int wp = 0;
        int bp = 0;
        while (!playersSet) {
            try {
                System.out.println("Which type will be the white player? (1: Real person behind this PC; 2: Random player; 3: Remote player)\n");
                wp = in.nextInt();
                System.out.println("Which type will be the black player? (1: Real person behind this PC; 2: Random player; 3: Remote player)\n");
                bp = in.nextInt();
                if ((wp == 1 && bp == 1) || (wp == 1 && bp == 2) || (wp == 1 && bp == 3) || (wp == 2 && bp == 1) || (wp == 3 && bp == 1)) {
                    playersSet = true;
                    InputController controller = new InputController(wp, bp);
                    if (wp == 3 || bp == 3) {
                        while (!establishServer((wp == 1 ? Team.WHITE : Team.BLACK))) {

                        }
                    }
                    System.out.println(controller);
                } else {
                    System.out.println("Try again...\n");
                }
            } catch (Exception ex) {
                in.next();
                System.out.println("Try again...\n");
            }
        }
    }
    /**
    * Lets the user set time limit
    */
    static void customizeTime() {
        Scanner in = new Scanner(System.in);
        Boolean timeSet = false;
        while (!timeSet) {
            System.out.println("Set time limit of both players: \n");
            whiteTime = in.nextInt();
            blackTime = whiteTime;
            timeSet = true;
        }
    }
   /**
    * Starts server and client for online game
    * @param localColor color of player behind this computer
    * @return true if succesfull
    */
    static Boolean establishServer(Team localColor) {
        Scanner in = new Scanner(System.in);
        System.out.println("IP adress of this PC: \n");
        String serverIP = in.nextLine();
        String port;
        try {
            server = new Server(serverIP);
            server.startServer();
            serverExists = true;
        } catch (Exception ex) {
            System.out.println("Wrong IP, try again\n");
            return false;
        }
        System.out.println("IP adress of another PC: \n");
        String clientIP = in.nextLine();
        System.out.println("Another PCs receivers port: \n");
        port = in.nextLine();
        try {
            client = new Client(
                InetAddress.getByName(clientIP), Integer.parseInt(port));
                client.startClient(/*clientIP, port*/);
                clientExists = true;
        } catch (Exception ex) {
            System.out.println("Wrong client IP or port, try again\n");
            return false;
        }
        System.out.println("Server established\n");
        return true;
    }
    /**
     * Relocate a piece. Divided into two phases. In the first phase is established, 
     * which piece will be relocated. In the second, the chosen piece is relocated to
     * target destination.
     * @param pos position of piece which should be relocated / its destination
     */
    static void relocate(Coord pos) {
        if(!pieceToRelocateChosen) {
            if (!chessboard[pos.x][pos.y].isReal) {
            } else {
                chosenPieceToRelocate = pos;
                pieceToRelocateChosen = true;
            }
        }
        else {
            chessboard[pos.x][pos.y] = chessboard[chosenPieceToRelocate.x][chosenPieceToRelocate.y];
            chessboard[chosenPieceToRelocate.x][chosenPieceToRelocate.y] = new Placeholder(Team.WHITE, "Placeholder", Type.PLACEHOLDER, false, false);
            if (chessboard[pos.x][pos.y].isReal) {
                FXMain.removePiece(pos);
            }
            FXMain.movePiece(chosenPieceToRelocate, pos);
            chessboard[pos.x][pos.y].hasMoved = true;
            pieceToRelocateChosen = false;
        }
    }
    /**
    * Moves with a piece. In the first phase it is established which piece will
    * be moved witch. In the second one it tries to move that piece into target
    * destinatoin (may or may not be successfull)
    * @param pos position of piece which should be moved / its destination
    */
    static void move(Coord pos) {
        if (gameRunning) {
            if(!pieceChosen) {
                if (!chessboard[pos.x][pos.y].isReal) {
                    
                } else if (chessboard[pos.x][pos.y].color != whoseTurn) {
                    
                } else {
                    moves = checkMoves(pos, whoseTurn);
                    chosenPiece = pos;
                    pieceChosen = true;
                }
            }
            else {
                if (moves.contains(pos)) {
                    if (chessboard[pos.x][pos.y].type == Type.KING) {
                        endGame(chessboard[pos.x][pos.y].color);
                    }
                    chessboard[pos.x][pos.y] = chessboard[chosenPiece.x][chosenPiece.y];
                    chessboard[chosenPiece.x][chosenPiece.y] = new Placeholder(Team.WHITE, "Placeholder", Type.PLACEHOLDER, false, false);
                    
                    // does it get into check?
                    if (checkCheck(whoseTurn, findPiece(whoseTurn, Type.KING), false, false)) {
                        chessboard[chosenPiece.x][chosenPiece.y] = chessboard[pos.x][pos.y];
                        chessboard[pos.x][pos.y] = new Placeholder(Team.WHITE, "Placeholder", Type.PLACEHOLDER, false, false);
                        pieceChosen = false;
                    } else {
                        // en passant
                        if (enPassant != null) {
                            if (chessboard[pos.x][pos.y].type == Type.PAWN &&
                                pos.y == enPassant.y) {
                                if (chessboard[pos.x][pos.y].color == Team.WHITE &&
                                        pos.y == 2) {
                                    chessboard[pos.x][3] = new Placeholder(Team.WHITE, "Placeholder", Type.PLACEHOLDER, false, false);
                                    FXMain.removePiece(new Coord(pos.x, 3));
                                }
                                if (chessboard[pos.x][pos.y].color == Team.BLACK &&
                                        pos.y == 5) {
                                    chessboard[pos.x][4] = new Placeholder(Team.WHITE, "Placeholder", Type.PLACEHOLDER, false, false);
                                    FXMain.removePiece(new Coord(pos.x, 4));
                                }
                            }
                        }
                        enPassant = null;
                        if (chessboard[pos.x][pos.y].type == Type.PAWN) {
                            if (pos.y == 3 && chosenPiece.y == 1) {
                                enPassant = new Coord(pos.x, 2);
                            } else if (pos.y == 4 && chosenPiece.y == 6) {
                                enPassant = new Coord(pos.x, 5);
                            }
                        }
                        if (chessboard[pos.x][pos.y].isReal) {
                            
                            FXMain.removePiece(pos);
                        }
                        FXMain.movePiece(chosenPiece, pos);
                        // castling
                        if (chessboard[pos.x][pos.y].type == Type.KING && 
                                (pos.x == 2 || pos.x == 6)) {
                            if (pos.x == 2 && pos.y == 0) {
                                relocate(new Coord(0, 0));
                                relocate(new Coord(3, 0));
                            }
                            if (pos.x == 6 && pos.y == 0) {
                                relocate(new Coord(7, 0));
                                relocate(new Coord(5, 0));
                            }
                            if (pos.x == 2 && pos.y == 7) {
                                relocate(new Coord(0, 7));
                                relocate(new Coord(3, 7));
                            }
                            if (pos.x == 6 && pos.y == 7) {
                                relocate(new Coord(7, 7));
                                relocate(new Coord(5, 7));
                            }
                         
                        }
                        // promotion
                        
                        if (chessboard[pos.x][pos.y].type == Type.PAWN && 
                                (pos.y == 0 || pos.y == 7)) {
                            promotionDeclaration(pos);
                        }
                        
                        chessboard[pos.x][pos.y].hasMoved = true;
                        pieceChosen = false;
                        whoseTurn = (whoseTurn == Team.WHITE ? Team.BLACK : Team.WHITE);
                        // is enemy under check?
                        if (checkCheck(whoseTurn, findPiece(whoseTurn, Type.KING), false, true)) {
                            if (whoseTurn == Team.WHITE) {
                                whiteCheck = true;
                            } else {
                                blackCheck = true;
                            }
                            if (checkMate(whoseTurn)) {
                                System.out.println("Game is over");
                                endGame(whoseTurn);
                            }
                        }
                    }
                } else {
                    pieceChosen = false;
                }
            }
        }
    }
    /**
     * Ends the game if the king is captured or one of the players has no time left
     * @param loserColor color of losing player
     */
    static void endGame(Team loserColor) {
        FXMain.endGame(whoseTurn);
        gameRunning = false;
    }
    /**
     * Asks the user which piece does he want the pawn to be promoted to,
     * calls promotionExecution if necessary
     * @param coord Coord of the pawn ready to be promoted
     */
    static void promotionDeclaration(Coord coord) {
        toBePromoted = coord;
        if ((InputController.whitePlayer == TypeOfPlayer.THISPC && chessboard[coord.x][coord.y].color == Team.WHITE) || 
                (InputController.blackPlayer == TypeOfPlayer.THISPC && chessboard[coord.x][coord.y].color == Team.BLACK)) {
            Scanner sc = new Scanner(System.in);
            Boolean typeChosen = false;
            System.out.println("Which piece do you want the pawn to be promoted to? (1: Queen, 2: Rook, 3: Bishop, 4: Knight\n");
            int choice;
            while (!typeChosen) {
                choice = sc.nextInt();
                if (choice >= 1 && choice <= 4) {
                    typeChosen = true;
                    promotionExecution(choice);
                    if (InputController.blackPlayer == TypeOfPlayer.REMOTEPC ||
                            InputController.whitePlayer == TypeOfPlayer.REMOTEPC) {
                        InputController.sender(new Coord(9, choice));
                    }
                    
                } else {
                    System.out.println("Try again...\n");
                }
            }
            
        } else {
            waitingForServer = true;
        }
        
    }
    /**
     * Executes the promotion - replaces the pawn with a chosen piece
     * @param choice number representing which piece will be the pawn transformed into
     */
    static void promotionExecution(int choice) {
        if (toBePromoted != null) {
            Team designatedTeam = chessboard[toBePromoted.x][toBePromoted.y].color;
            if (choice == 1) {
                    chessboard[toBePromoted.x][toBePromoted.y] = new Queen(designatedTeam, "queen", Type.QUEEN, true, true);
            } else if (choice == 2) {
                    chessboard[toBePromoted.x][toBePromoted.y] = new Rook(designatedTeam, "queen", Type.ROOK, true, true);
            } else if (choice == 3) {
                    chessboard[toBePromoted.x][toBePromoted.y] = new Bishop(designatedTeam, "queen", Type.BISHOP, true, true);
            } else if (choice == 4) {
                    chessboard[toBePromoted.x][toBePromoted.y] = new Knight(designatedTeam, "queen", Type.KNIGHT, true, true);
            }
            FXMain.restart();
            toBePromoted = null;
            waitingForServer = false;
        } else {
            System.out.println("No piece to be promoted");
        }
        
    }
    /**
     * Ask target piece for possible moves. Than it reduces the list depending on
     * current situation on the chessboard (some ways are blocked by another pieces)
     * @param pos position of the chosen piece
     * @param team chosen pieces team
     * @return 
     */
    static List checkMoves(Coord pos, Team team) {
        List<Coord> ret = new ArrayList<>();
        Coord[][] pos_moves;
        pos_moves = chessboard[pos.x][pos.y].getMoves(pos);
        int len1 = pos_moves.length;
        if (chessboard[pos.x][pos.y].type == Type.KING) {
            ret.addAll(Arrays.asList(pos_moves[8]));

            len1 = len1 - 1;
        }

        for (int i = 0; i < len1; ++i) {
            for (int y = 0; y < pos_moves[i].length; ++y) {

                if (pos_moves[i][y] == null) {
                    break;
                } else if (!chessboard[pos_moves[i][y].x][pos_moves[i][y].y].isReal) {
                    ret.add(pos_moves[i][y]);
                } else {
                    if (chessboard[pos_moves[i][y].x][pos_moves[i][y].y].color == team) {
                        break;
                    } else if (chessboard[pos_moves[i][y].x][pos_moves[i][y].y].color != team) {
                        ret.add(pos_moves[i][y]);
                        break;
                    }
                }
            }
        }
        return ret;
    }
    /**
     * Returns true if chosen team is in check mate
     * @param team team which might be under check
     * @return true if its check mate, false otherwise
     */
    static Boolean checkMate(Team team) {
        Team otherTeam = (team == Team.WHITE ? Team.BLACK : Team.WHITE);
        Coord[][] allPathes = chessboard[threat.x][threat.y].getMoves(threat);
        List<Coord> toBeBlocked = new ArrayList<>();
        List<Coord> enemyPossibilities = new ArrayList<>();
        Boolean pathFound = false;
        Coord kingPos = findPiece(team, Type.KING);
        // finds a path tho the king
        for(Coord[] curPath : allPathes) {
            if (Arrays.asList(curPath).contains(kingPos)) {
                for (Coord pos : curPath) {
                    if (pos != null && kingPos != null) {
                        if (pos.equals(kingPos)) {
                            pathFound = true;
                        } else {
                            toBeBlocked.add(pos);
                        }
                    }
                }
            }
        }
        for (Coord pos : toBeBlocked) {
            if (checkCheck(otherTeam, pos, true, false)) {
                return false;
            }
        }
        List<Coord> kingPossibilities = checkMoves(kingPos, team);
        for (Coord possibility : kingPossibilities) {
            if (!checkCheck(team, possibility, false, false) && possibility != null) {
                return false;
            }
        }
        return true;
    }
    /**
     * Returns list of tiles endangered by target piece
     * @param pos position of the dangerous piece
     * @param team the other team
     * @return list of tiles endangered by target piece
     */
    static List checkEndangered(Coord pos, Team team) {
        List<Coord> ret = new ArrayList<>();
        if (chessboard[pos.x][pos.y].type != Type.PAWN && chessboard[pos.x][pos.y].isReal) {
            Coord[][] pos_moves;
            pos_moves = chessboard[pos.x][pos.y].getMoves(pos);
            
            for (int i = 0; i < pos_moves.length; ++i) {
                for (int y = 0; y < pos_moves[i].length; ++y) {

                    if (pos_moves[i][y] == null) {
                        break;
                    } else if (!chessboard[pos_moves[i][y].x][pos_moves[i][y].y].isReal) {
                        ret.add(pos_moves[i][y]);
                    } else {
                        if (chessboard[pos_moves[i][y].x][pos_moves[i][y].y].color != team) {
                            break;
                        } else if (chessboard[pos_moves[i][y].x][pos_moves[i][y].y].color == team) {
                            ret.add(pos_moves[i][y]);
                            break;
                        }
                    }
                }
            }
        } else {
            if (chessboard[pos.x][pos.y].color == Team.BLACK) {
                ret.add(new Coord(pos.x + 1, pos.y + 1));
                ret.add(new Coord(pos.x - 1, pos.y + 1));
            } else {
                ret.add(new Coord(pos.x + 1, pos.y - 1));
                ret.add(new Coord(pos.x - 1, pos.y - 1));
            }
        }
        return ret;
    }
    /**
     * Returns true when chosen tile is endangered by ANOTHER team (not only check).
     * justPos is true when the side moves of pawns doesnt need to be counted
     * resetThreat is true when the threat variable does not need to be updated
     * @param team the dangerous team
     * @param pos position of the possibly endangered piece
     * @param justPos true if nonlethal moves are required (because of pawns)
     * @param resetThreat true if the treat should be reseted
     * @return true if the pos is endangered
     */
    static Boolean checkCheck(Team team, Coord pos, Boolean justPos, Boolean resetThreat) {
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                if (chessboard[x][y].isReal && chessboard[x][y].color != team) {
                    if (justPos) {
                        if (checkMoves(new Coord(x, y), team).contains(pos)
                            && chessboard[x][y].type != Type.KING) {
                            if (resetThreat) {
                                threat = new Coord(x, y);
                            }
                            return true;
                        }
                    } else {
                        if (checkEndangered(new Coord(x, y), team).contains(pos)
                                && chessboard[x][y].type != Type.KING) {
                            if (resetThreat) {
                                threat = new Coord(x, y);
                            }
                            
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
   /**
    * Finds the first piece of chosen team and type it comes across
    * @param team team of the required piece
    * @param type type of required piece
    * @return coordinates of required piece
    */
    private static Coord findPiece(Team team, Type type) {
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                if (chessboard[x][y].type == type && chessboard[x][y].color == team) {
                    return new Coord(x, y);
                }
            }
        }
        return null;
    }
    /**
    * Fills the board with pieces (the default way)
    */
    private static void fillBoard() {
        for(int x = 0; x < 8; ++x) {
            for(int y = 0; y < 8; ++y) {
                chessboard[x][y] = new Placeholder(Team.WHITE, "Placeholder", Type.PLACEHOLDER, false, false);
            }
        }
        for (int i = 0; i <= 7; ++i) {
            String bname = "bp" + i;
            String wname = "wp" + i;
            chessboard[i][1] = new Pawn(Team.BLACK, bname, Type.PAWN, false, true);
            chessboard[i][6] = new Pawn(Team.WHITE, wname, Type.PAWN, false, true);
        }
        chessboard[0][0] = new Rook(Team.BLACK, "br1", Type.ROOK, false, true);
        chessboard[1][0] = new Knight(Team.BLACK, "bk1", Type.KNIGHT, false, true);
        chessboard[2][0] = new Bishop(Team.BLACK, "bb1", Type.BISHOP, false, true);
        chessboard[4][0] = new King(Team.BLACK, "bking", Type.KING, false, true);
        chessboard[3][0] = new Queen(Team.BLACK, "bqueen", Type.QUEEN, false, true);
        chessboard[5][0] = new Bishop(Team.BLACK, "bb2", Type.BISHOP, false, true);
        chessboard[6][0] = new Knight(Team.BLACK, "bk2", Type.KNIGHT, false, true);
        chessboard[7][0] = new Rook(Team.BLACK, "br2", Type.ROOK, false, true);
        
        chessboard[0][7] = new Rook(Team.WHITE, "wr1", Type.ROOK, false, true);
        chessboard[1][7] = new Knight(Team.WHITE, "wk1", Type.KNIGHT, false, true);
        chessboard[2][7] = new Bishop(Team.WHITE, "wb1", Type.BISHOP, false, true);
        chessboard[4][7] = new King(Team.WHITE, "wking", Type.KING, false, true);
        chessboard[3][7] = new Queen(Team.WHITE, "wqueen", Type.QUEEN, false, true);
        chessboard[5][7] = new Bishop(Team.WHITE, "wb2", Type.BISHOP, false, true);
        chessboard[6][7] = new Knight(Team.WHITE, "wk2", Type.KNIGHT, false, true);
        chessboard[7][7] = new Rook(Team.WHITE, "wr2", Type.ROOK, false, true);
    }
}
