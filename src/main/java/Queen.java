

/**
 * Queen piece, see AbstractPiece
 * @author kraus
 */
public class Queen extends AbstractPiece {

    public Queen(Team color, String name, Type type, Boolean hasMoved, Boolean isReal) {
        super(color, name, type, hasMoved, isReal);
    }

    @Override
    Coord[][] getMoves(Coord coord) {
        /*super.getMoves(coord);*/
        Coord[] n = new Coord[8];
        Coord[] e = new Coord[8];
        Coord[] w = new Coord[8];
        Coord[] s = new Coord[8];
        Coord[] n2 = new Coord[8];
        Coord[] e2 = new Coord[8];
        Coord[] w2 = new Coord[8];
        Coord[] s2 = new Coord[8];
        for (int i = 0; i < 8; ++i) {
            if (coord.y + (i + 1) <= 7) {
                n[i] = new Coord(coord.x, coord.y + (i + 1));
            }
            if (coord.x + (i + 1) <= 7) {
                e[i] = new Coord(coord.x + (i + 1), coord.y);
            }
            if (coord.x - (i + 1) >= 0) {
                w[i] = new Coord(coord.x - (i + 1), coord.y);
            }
            if (coord.y - (i + 1) >= 0) {
                s[i] = new Coord(coord.x, coord.y - (i + 1));
            }
            if (coord.y + (i + 1) <= 7 && coord.x + (i + 1) <= 7) {
                n2[i] = new Coord(coord.x + (i + 1), coord.y + (i + 1));
            }
            if (coord.y - (i + 1) >= 0 && coord.x + (i + 1) <= 7) {
                e2[i] = new Coord(coord.x + (i + 1), coord.y - (i + 1));
            }
            if (coord.y + (i + 1) <= 7 && coord.x - (i + 1) >= 0) {
                w2[i] = new Coord(coord.x - (i + 1), coord.y + (i + 1));
            }
            if (coord.y - (i + 1) >= 0 && coord.x - (i + 1) >= 0) {
                s2[i] = new Coord(coord.x - (i + 1), coord.y - (i + 1));
            }
        }
        return new Coord[][]{n, e, w, s, n2, e2, w2, s2};
    }
}