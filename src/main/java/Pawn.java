/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Represents pawn. See AbstractPiece
 * @author kraus
 */
public class Pawn extends AbstractPiece {

    public Pawn(Team color, String name, Type type, Boolean hasMoved, Boolean isReal) {
        super(color, name, type, hasMoved, isReal);
    }
    
    @Override
    Coord[][] getMoves(Coord coord) {
        // move straight
        Coord[] move = new Coord[2];
        // capture enemy piece
        Coord[] left = new Coord[1];
        Coord[] right = new Coord[1];
        if (color == Team.WHITE) {
            if (coord.y - 1 >= 0) {
                if (!Main.chessboard[coord.x][coord.y - 1].isReal) {
                    move[0] = new Coord(coord.x, coord.y - 1);
                    if (!hasMoved && !Main.chessboard[coord.x][coord.y - 2].isReal) {
                        move[1] = new Coord(coord.x, coord.y - 2);
                    }
                }
            }
            if (coord.x > 0 && coord.y > 0) {
                if ((Main.chessboard[coord.x - 1][coord.y - 1].color == Team.BLACK &&
                        Main.chessboard[coord.x - 1][coord.y - 1].isReal) || 
                        ((new Coord(coord.x - 1, coord.y - 1)).equals(Main.enPassant))) {
                    left[0] = new Coord(coord.x - 1, coord.y - 1);
                }
            }
            if (coord.x < 7 && coord.y > 0) {
                if ((Main.chessboard[coord.x + 1][coord.y - 1].color == Team.BLACK &&
                        Main.chessboard[coord.x + 1][coord.y - 1].isReal) || 
                        ((new Coord(coord.x + 1, coord.y - 1)).equals(Main.enPassant))) {
                    right[0] = new Coord(coord.x + 1, coord.y - 1);
                }
            }
        } else {
            if (coord.y < 7) {
                if (!Main.chessboard[coord.x][coord.y + 1].isReal) {
                    move[0] = new Coord(coord.x, coord.y + 1);
                    if (!hasMoved && !Main.chessboard[coord.x][coord.y + 2].isReal) {
                        move[1] = new Coord(coord.x, coord.y + 2);
                    }
                }
            }
            if (coord.x > 0 && coord.y < 7) {
                if ((Main.chessboard[coord.x - 1][coord.y + 1].color == Team.WHITE &&
                        Main.chessboard[coord.x - 1][coord.y + 1].isReal) || 
                        ((new Coord(coord.x - 1, coord.y + 1)).equals(Main.enPassant))) {
                    left[0] = new Coord(coord.x - 1, coord.y + 1);
                }
            }
            if (coord.x < 7 && coord.y < 7) {
                if ((Main.chessboard[coord.x + 1][coord.y + 1].color == Team.WHITE &&
                        Main.chessboard[coord.x + 1][coord.y + 1].isReal) || 
                        ((new Coord(coord.x + 1, coord.y + 1)).equals(Main.enPassant)) ){
                    right[0] = new Coord(coord.x + 1, coord.y + 1);
                }
            }
        }
        return new Coord[][]{move, left, right};
    }
}
