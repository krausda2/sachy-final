


/**
 * Redirects the four rectangle buttons depending on their ID
 * @author kraus
 */
public class ButtonController {
    static Timer timer = new Timer();
    static Boolean timerExists = false;
    
    /**
     * This method is called by the button handlers, they pass it their ID and this
     * method calls the required method
     * @param id ID of the button
     */
    static void redirectMe(int id) {
        
        if (id == 0) {
            Main.switchAdjusting();
        }
        if (id == 1) {
            Main.saveGame();
        }
        if (id == 2) {
            Main.loadGame();
        }
        if (id == 3) {
            
            if (!timerExists) {
                timer.start();
            }
            if (Main.clientExists) {
                InputController.sender(new Coord(8, 8));
            }
            Main.gameRunning = !Main.gameRunning;
            timerExists = true;
            if (InputController.whitePlayer == TypeOfPlayer.RANDOM) {
                InputController.moveRequester(Team.WHITE);
            }
        }
    }
}
