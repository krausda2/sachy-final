import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;




/**
 * Server that recieves data from client.
 * @author david
 */
public class Server implements Runnable{
    private ServerSocket server;
    private Thread t;
    private String ip;
    BufferedReader in;
    String data = null;
    /**
     * Initialises a receiver
     * @param ipAddress This pcs ip
     * @throws Exception 
     */
    Server(String ipAddress) throws Exception {
        if (ipAddress != null && !ipAddress.isEmpty()) 
          this.server = new ServerSocket(0, 1, InetAddress.getByName(ipAddress));
        else 
          this.server = new ServerSocket(0, 1, InetAddress.getLocalHost());
    }
    /**
     * stops this tread
     */
    void stop() {
        try {
            server.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        Thread moribund = t;
        t = null;
        moribund.interrupt();
    }
    
    @Override
    /**
     * Runs the server
     */
    public void run(){
        Queue<String> queue = new LinkedList<>();
        try {
            
            Socket client = this.server.accept();
            String clientAddress = client.getInetAddress().getHostAddress();
            System.out.println("\r\nNew connection from " + clientAddress);
            
            in = new BufferedReader(
                    new InputStreamReader(client.getInputStream()));
            while ( (data = in.readLine()) != null ) {
                /*System.out.println("\r\nMessage from " + clientAddress + ": " + data);*/
                Platform.runLater(new Runnable() {
                    @Override public void run() {
                        interpretMove(data);
                    }
                });
            }
        } catch (IOException ex) {
            System.out.println("error with receiving");
        }
        
    }
    /**
     * decides whether the received message represents a coordinates or not, then
     * executes the required action (move / promotion / start timer)
     * @param input Coordinates as string
     */
    void interpretMove(String input) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Coord coord = objectMapper.readValue(input, Coord.class);
            if (coord.x == 8) {
                if (!ButtonController.timerExists) {
                    ButtonController.timer.start();
                    ButtonController.timerExists = true;
                }
                Main.gameRunning = !Main.gameRunning;
                ButtonController.timerExists = true;
            } else if (coord.x == 9) {
                Main.promotionExecution(coord.y);
            } else {
                Team remoteColor = (InputController.whitePlayer == TypeOfPlayer.REMOTEPC ? Team.WHITE : Team.BLACK);
                InputController.move(TypeOfPlayer.REMOTEPC, remoteColor, coord);
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * gets this PCs IP address
     * @return this PCs IP
     */
    InetAddress getSocketAddress() {
        return this.server.getInetAddress();
    }
    /**
     * gts this receivers port
     * @return receivers port
     */
    int getPort() {
        return this.server.getLocalPort();
    }
    /**
     * Starts the server, prints its port and IP
     * @throws Exception 
     */
    void startServer() throws Exception {
        System.out.println("\r\nRunning Server: " + 
                "Host=" + getSocketAddress().getHostAddress() + 
                " Port=" + getPort());
        
        start();
    }
    /**
     * starts the thread
     * @throws IOException 
     */
    private void start() throws IOException {
        if (t == null) {
         t = new Thread (this);
         t.start ();
      }
    }
}