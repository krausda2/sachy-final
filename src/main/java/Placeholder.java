/**
 * Placeholder used to fill empty tiles of chessboard rather than leave them empty.
 * @author david
 */
public class Placeholder extends AbstractPiece {

    public Placeholder(Team color, String name, Type type, Boolean hasMoved, Boolean isReal) {
        super(color, name, type, hasMoved, isReal);
    }
    

    @Override
    Coord[][] getMoves(Coord coord) {
        throw new UnsupportedOperationException("Just a placeholder."); //To change body of generated methods, choose Tools | Templates.
    }
}
