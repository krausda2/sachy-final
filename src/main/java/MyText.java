
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;



/**
 * Graphical representation of text on buttons
 * @author kraus
 */
public class MyText extends Text {
    public int coordX;
    public int coordY;
    public int id;
    public String str;
    /**
     * Sets the texts position, text, font and color
     */
    void findYourPlace() {
        relocate(coordX, coordY);
        setText(str);
        setFont(Font.font ("Verdana", 19));
        setFill(Color.WHEAT);
    }
    /**
     * Updates the text
     * @param newString the new text 
     */
    void update(String newString) {
        setText(str);
    }
    public MyText(int x, int y, int id, String fill) {
        this.coordX = x + 15;
        this.coordY = y + 60;
        this.str = fill;
        setOnMousePressed((MouseEvent click) -> {
            ButtonController.redirectMe(id);
        });
    }
}
