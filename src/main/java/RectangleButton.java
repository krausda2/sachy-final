import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;



/**
 * Button which extends rectangle. Redirected to ButtonControllr when clicked.
 * @author kraus
 */
public class RectangleButton extends Rectangle {
    public int coordX;
    public int coordY;
    public int id;
    
    /**
    * Sets its fill and relocates itself depending on its coords.
    */
    void findYourPlace() {

        setFill(Color.MAROON);
        relocate(coordX, coordY);
    }
    
    /**
    * Constructor
     * @param x x position
     * @param y y position
     * @param id buttons id
    */
    public RectangleButton(int x, int y, int id) {
        this.coordX = x;
        this.coordY = y;
        setWidth(FXMain.SIZE);
        setHeight(FXMain.SIZE * 2 - 6);
        setOnMousePressed((MouseEvent click) -> {
            ButtonController.redirectMe(id);
        });
    }
    
    
}
