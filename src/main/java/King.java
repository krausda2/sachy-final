
import java.util.Arrays;



/**
 * King piece, see AbstractPiece
 * @author kraus
 */
public class King extends AbstractPiece {

    public King(Team color, String name, Type type, Boolean hasMoved, Boolean isReal) {
        super(color, name, type, hasMoved, isReal);
    }
    
    @Override
    /**
    * getMoves includes castling
    */
    Coord[][] getMoves(Coord coord) {
        
        /*super.getMoves(coord);*/
        Coord[] n = new Coord[1];
        Coord[] e = new Coord[1];
        Coord[] w = new Coord[1];
        Coord[] s = new Coord[1];
        Coord[] n2 = new Coord[1];
        Coord[] e2 = new Coord[1];
        Coord[] w2 = new Coord[1];
        Coord[] s2 = new Coord[1];
        Coord[] castling = new Coord[2];
        /*
        * Checks if castling is possible
        */
        if (!hasMoved && coord.y == 0) {
            if (Main.chessboard[7][0].type == Type.ROOK) {
                if (!Main.chessboard[5][0].isReal && !Main.chessboard[6][0].isReal) {
                    castling[0] = new Coord(6, 0);
                }
            }
            if (Main.chessboard[0][0].type == Type.ROOK) {
                if (!Main.chessboard[1][0].isReal && !Main.chessboard[2][0].isReal && !Main.chessboard[3][0].isReal) {
                    castling[1] = new Coord(2, 0);
                }
            }
        }
        
        if (!hasMoved && coord.y == 7) {
            if (Main.chessboard[7][7].type == Type.ROOK) {
                if (!Main.chessboard[5][7].isReal && !Main.chessboard[6][7].isReal) {
                        castling[0] = new Coord(6, 7);
                }
            }
            if (Main.chessboard[0][7].type == Type.ROOK) {
                if (!Main.chessboard[1][7].isReal && !Main.chessboard[2][7].isReal && !Main.chessboard[3][7].isReal) {
                        castling[1] = new Coord(2, 7);
                }
            }
        }
        /*
        * checks regular moves
        */
        if (coord.y + 1 <= 7) {
            n[0] = new Coord(coord.x, coord.y + 1);
        }
        if (coord.x + 1 <= 7) {
            e[0] = new Coord(coord.x + 1, coord.y);
        }
        if (coord.x - 1 >= 0) {
            w[0] = new Coord(coord.x - 1, coord.y);
        }
        if (coord.y - 1 >= 0) {
            s[0] = new Coord(coord.x, coord.y - 1);
        }
        if (coord.y + 1 <= 7 && coord.x + 1 <= 7) {
            n2[0] = new Coord(coord.x + 1, coord.y + 1);
        }
        if (coord.y - 1 >= 0 && coord.x + 1 <= 7) {
            e2[0] = new Coord(coord.x + 1, coord.y - 1);
        }
        if (coord.y + 1 <= 7 && coord.x - 1 >= 0) {
            w2[0] = new Coord(coord.x - 1, coord.y + 1);
        }
        if (coord.y - 1 >= 0 && coord.x - 1 >= 0) {
            s2[0] = new Coord(coord.x - 1, coord.y - 1);
        }
        return new Coord[][]{n, e, w, s, n2, e2, w2, s2, castling};
    }
}