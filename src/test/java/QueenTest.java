/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kraus
 */
public class QueenTest {
    
    public QueenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMoves method, of class Queen.
     */
    @Test
    public void testGetMoves1() {
        System.out.println("getMoves1");
        Coord coord = new Coord(0, 0);
        Queen instance = new Queen(Team.WHITE, "q", Type.QUEEN, true, true);
        Coord[][] expResult = new Coord[][]{{new Coord(0, 1), new Coord(0, 2), new Coord(0, 3), new Coord(0, 4), new Coord(0, 5), new Coord(0, 6), new Coord(0, 7), null}, 
            {new Coord(1, 0), new Coord(2, 0), new Coord(3, 0), new Coord(4, 0), new Coord(5, 0), new Coord(6, 0), new Coord(7, 0), null}, 
            {null, null, null, null, null, null, null, null}, {null, null, null, null, null, null, null, null}, 
            {new Coord(1, 1), new Coord(2, 2), new Coord(3, 3), new Coord(4, 4), new Coord(5, 5), new Coord(6, 6), new Coord(7, 7), null}, 
            {null, null, null, null, null, null, null, null}, {null, null, null, null, null, null, null, null}, {null, null, null, null, null, null, null, null}};
        System.out.println(Arrays.deepToString(expResult));
        Coord[][] result = instance.getMoves(coord);
        System.out.println(Arrays.deepToString(result));
        assertEquals(expResult, result);
    }
    
}
