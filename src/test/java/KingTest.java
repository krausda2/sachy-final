/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kraus
 */
public class KingTest {
    
    public KingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMoves method, of class King.
     */
    @org.junit.Test
    public void testGetMoves1() {
        System.out.println("getMoves");
        Coord coord = (new Coord(5, 5));
        King instance = new King(Team.BLACK, "k", Type.KING, true, true);
        Coord[][] expResult = new Coord[][]{{new Coord(5, 6)}, {new Coord(6, 5)}
        , {new Coord(4, 5)}, {new Coord(5, 4)}, {new Coord(6, 6)}, {new Coord(6, 4)}
        , {new Coord(4, 6)}, {new Coord(4, 4)}, {null, null}};
        System.out.println(Arrays.deepToString(expResult));
        Coord[][] result = instance.getMoves(coord);
        System.out.println(Arrays.deepToString(result));
        assertEquals(expResult, result);
    }
    public void testGetMoves2() {
        System.out.println("getMoves2");
        Coord coord = (new Coord(0, 0));
        King instance = new King(Team.BLACK, "k", Type.KING, true, true);
        Coord[][] expResult = new Coord[][]{{new Coord(0, 1)}, {new Coord(1, 0)}
        , {null}, {null}, {new Coord(1, 1)}, {null}
        , {null}, {null}, {null, null}};
        System.out.println(Arrays.deepToString(expResult));
        Coord[][] result = instance.getMoves(coord);
        System.out.println(Arrays.deepToString(result));
        assertEquals(expResult, result);
    }
    public void testGetMoves3() {
        System.out.println("getMoves2");
        Coord coord = (new Coord(4, 7));
        King instance = new King(Team.WHITE, "k", Type.KING, false, true);
        Coord[][] expResult = new Coord[][]{{null}, {new Coord(5, 7)}
        , {new Coord(3, 7)}, {new Coord(4, 6)}, {null}, {new Coord(5, 6)}
        , {null}, {new Coord(5, 6)}, {new Coord(2, 7), new Coord(6, 7)}};
        System.out.println(Arrays.deepToString(expResult));
        Coord[][] result = instance.getMoves(coord);
        System.out.println(Arrays.deepToString(result));
        assertEquals(expResult, result);
    }
    
}
